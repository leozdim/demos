const AWS = require("aws-sdk");
const dotenv = require("dotenv").config();
const fs = require("fs");
const rekognition = new AWS.Rekognition({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
  apiVersion: "20190518"
});

//const images = ['01.png', '02.png', '03.png', '04.png', '05.png']
const images = ["demo3.png"];
images.forEach(img => {
  let bitmap = fs.readFileSync(`./images/${img}`);
  let image = {
    Image: {
      Bytes: bitmap
    },
    MaxLabels: 123,
    MinConfidence: 60
  };
  rekognition.detectLabels(image, (error, data) => {
    if (error) {
      throw new Error(error);
    }
    data.Labels.forEach(item => {
      console.dir(JSON.stringify(item));
    });
    var params = {
      Image: {
        Bytes: bitmap
      }
    };
    //console.error("**Text detected**");
    rekognition.detectText(params, (err, data) => {
      if (err) {
        console.log(err, err.stack);
      }
      console.table(data.TextDetections);
    });
  });
});
