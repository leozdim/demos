const AWS = require("aws-sdk");
const dotenv = require("dotenv").config();
const fs = require("fs");
const rekognition = new AWS.Rekognition({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
    apiVersion: "20190518"
});

//const images = ['01.png', '02.png', '03.png', '04.png', '05.png']
const images = ["osmar.png"];
images.forEach(img => {
    let bitmap = fs.readFileSync(`./images/${img}`);
    let image = {
        Image: {
            Bytes: bitmap
        },
        Attributes: ["ALL"], //detectFaces
        //MaxLabels: 123, //detectLables
        //MinConfidence: 70 //detectLables
    };
    rekognition.detectFaces(image, (error, data) => {
        if (error) {
            throw new Error(error);
        }
        console.log(JSON.stringify(data.FaceDetails))
    });
});